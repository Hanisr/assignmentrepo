package ntuc.stepdefinitions;


import io.cucumber.java.Before;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;


import net.serenitybdd.screenplay.actors.OnStage;
import net.serenitybdd.screenplay.actors.OnlineCast;
import pages.NTUCAssignmentPage;


public class StepDefinitions extends NTUCAssignmentPage {

    @Before
    public void setTheStage() {
        OnStage.setTheStage(new OnlineCast());
    }

    @Given("user is on the webpage")
    public void user_is_on_the_webpage() {
        navigateToAssignmentPage();
    }

    @Given("user enters name: {string}")
    public void user_enters_name(String name) {
        enterName(name);
    }

    @Then("name {string} is displayed")
    public void name_is_displayed(String name) {
        verifyText(name);
    }

    @Given("user clicks on populate button")
    public void user_clicks_on_populate_button() {
        clickPopulateButton();
    }

    @Then("a name is displayed")
    public void a_name_is_displayed() {
        verifyText("Peter Parker");
    }

    @Given("user selects multiple answers")
    public void user_selects_multiple_answers() {
        selectCheckboxes();
    }

    @Then("checkboxes are selected")
    public void checkboxes_are_selected() {
        verifyCheckboxes();
    }

    @Given("user selects different operating system")
    public void user_selects_different_operating_system() {
        selectOS();
    }

    @Then("only one option is selected")
    public void only_one_option_is_selected() {
        verifyRadioButton();
    }

    @Given("user selects {string}")
    public void user_selects(String string) {
        selectDropdownValue(string);
    }

    @Then("{string} is selected")
    public void is_selected(String string) {
        verifyDropdownValue(string);
    }

    @Given("user has not tried TestCafe and does not select checkbox")
    public void user_has_not_tried_test_cafe_and_does_not_select_checkbox() {
        triedTestCafeCheckbox("not selected");
    }

    @Then("slider and comment box is disabled")
    public void slider_and_comment_box_is_disabled() {
        verifySliderAndComment("not selected");
    }

    @Given("user has tried TestCafe and selects checkbox")
    public void user_has_tried_test_cafe_and_selects_checkbox() {
        triedTestCafeCheckbox("selected");
    }
    @When("slider and comment box is enabled")
    public void slider_and_comment_box_is_enabled() {
        verifySliderAndComment("selected");
    }
    @Then("user rates TestCafe and enters feedback")
    public void user_rates_test_cafe_and_enters_feedback() {
        enterSliderAndComments();
    }

    @Given("user fills up the form and enters {string}")
    public void user_fills_up_the_form(String name) {
        fillUpForm(name);

    }
    @When("user submits the form")
    public void user_submits_the_form() {
        clickSubmit();
    }

    @Then("user {string} should see success message")
    public void user_should_see_success_message(String name) {
        verifySuccessMessage(name);
    }


}
