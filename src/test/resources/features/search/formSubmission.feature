Feature: feature to test form submission

  Background: User navigates to the webpage
    Given user is on the webpage

  Scenario Outline: Verify if user is able to fill his/her name in the form
    Given user enters name: '<name>'
    Then name '<name>' is displayed
    Examples:
      | name      |
      | Jane Kim  |
      | John Wick |

  Scenario: Verify if populate button is working
    Given user clicks on populate button
    Then a name is displayed

  Scenario: Verify if user can select multiple checkboxes
    Given user selects multiple answers
    Then checkboxes are selected

  Scenario: Verify if user is able to select only 1 option for radio buttons
    Given user selects different operating system
    Then only one option is selected

  Scenario Outline: Verify if default value for dropdown is correct and user is able to click on it
    Given user selects '<interface>'
    Then '<interface>' is selected
    Examples:
      | interface      |
      | Command Line   |
      | JavaScript API |
      | Both           |

  Scenario: Verify if slider and comment box is disabled when the checkbox is not selected
    Given user has not tried TestCafe and does not select checkbox
    Then slider and comment box is disabled

  Scenario: Verify if slider and comment box is enabled when the checkbox is selected
    Given user has tried TestCafe and selects checkbox
    When slider and comment box is enabled
    Then user rates TestCafe and enters feedback

  Scenario: Check whether user is able to submit form
    Given user fills up the form and enters "Tom"
    When user submits the form
    Then user "Tom" should see success message