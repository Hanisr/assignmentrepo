package pages;

import jxl.common.Assert;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;
import static org.junit.Assert.*;

import org.openqa.selenium.By;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;


public class NTUCAssignmentPage extends PageObject {

    @FindBy(xpath = "//header") private WebElementFacade headerText;
    @FindBy(xpath="//input[@id='developer-name']") private WebElementFacade displayedName;
    @FindBy(xpath="//input[@id='populate']") private WebElementFacade populateButton;
    @FindBy(xpath="//input[@id='remote-testing']") private WebElementFacade checkboxRemoteTest;
    @FindBy(xpath="//input[@id='reusing-js-code']") private WebElementFacade checkboxReuseJSCode;
    @FindBy(xpath="//input[@id='background-parallel-testing']") private WebElementFacade checkboxParallelTest;
    @FindBy(xpath="//input[@id='continuous-integration-embedding']") private WebElementFacade checkboxCI;
    @FindBy(xpath="//input[@id='traffic-markup-analysis']") private WebElementFacade checkboxTraffic;
    @FindBy(xpath="//input[@id='windows']") private WebElementFacade windowsOption;
    @FindBy(xpath="//input[@id='macos']") private WebElementFacade macOSOption;
    @FindBy(xpath="//input[@id='linux']") private WebElementFacade linuxOption;
    @FindBy(xpath="//select[@id='preferred-interface']") private WebElementFacade dropdownOptions;
    @FindBy(xpath="//input[@id='tried-test-cafe']") private WebElementFacade triedTestCafe;
    @FindBy(xpath="//div[@id='slider']") private WebElementFacade slider;
    @FindBy(xpath="//span[@class='ui-slider-handle ui-corner-all ui-state-default']") private WebElementFacade sliderHandle;
    @FindBy(xpath="//textarea[@id='comments']") private WebElementFacade commentsSection;
    @FindBy(xpath="//button[@id='submit-button']") private WebElementFacade submitButton;
    @FindBy(id = "article-header") private WebElementFacade submissionMessage;
    private String titlePage = "Example\nThis webpage is used as a sample in TestCafe tutorials.";


    protected void navigateToAssignmentPage() {
        openUrl("https://devexpress.github.io/testcafe/example/");
        assertEquals(headerText.getText(),titlePage);

    }

    protected void enterName(String name) {
        typeInto(displayedName, name);
    }

    protected void verifyText(String name) {
        String actualName = displayedName.getValue();
        assertEquals(actualName, name);
    }

    protected void clickPopulateButton() {
        clickOn(populateButton);
        getAlert().accept();
    }

    protected void selectCheckboxes() {
        clickOn(checkboxRemoteTest);
        clickOn(checkboxReuseJSCode);
        clickOn(checkboxTraffic);
    }

    protected void verifyCheckboxes() {
        assertTrue(checkboxRemoteTest.isSelected());
        assertTrue(checkboxReuseJSCode.isSelected());
        assertTrue(checkboxTraffic.isSelected());

    }

    protected void selectOS() {
        clickOn(macOSOption);
        clickOn(windowsOption);
        clickOn(linuxOption);
    }

    protected void verifyRadioButton() {
        assertTrue(linuxOption.isSelected());
    }

    protected void selectDropdownValue(String value) {
        assertTrue(dropdownOptions.getValue().equals("Command Line"));
        selectFromDropdown(dropdownOptions, value);

    }

    protected void verifyDropdownValue(String value) {
        assertTrue(dropdownOptions.getValue().equals(value));

    }

    protected void triedTestCafeCheckbox(String value) {
        if(value.equalsIgnoreCase("not selected")){
            assertFalse(triedTestCafe.isSelected());

        }else if(value.equalsIgnoreCase("selected")){
            clickOn(triedTestCafe);
            assertTrue(triedTestCafe.isSelected());
        }
    }

    protected void verifySliderAndComment(String value) {
        if(value.equalsIgnoreCase("not selected")){
            assertTrue(commentsSection.isDisabled());
            assertTrue(slider.getAttribute("class").contains("disabled"));
        }else if(value.equalsIgnoreCase("selected")){
            assertTrue(slider.isClickable());
            assertTrue(commentsSection.isEnabled());
        }
    }

    protected void enterSliderAndComments(){
        withAction().clickAndHold(sliderHandle).moveByOffset(700,0).build().perform();
        typeInto(commentsSection, "Very cool and highly recommended");
    }

    protected void fillUpForm(String name){
        waitFor(ExpectedConditions.visibilityOf(displayedName));
        enterName(name);
        selectCheckboxes();
        selectDropdownValue("Both");
        triedTestCafeCheckbox("selected");
        enterSliderAndComments();
    }

    protected void clickSubmit() {
        clickOn(submitButton);
    }

    protected void verifySuccessMessage(String name) {
        assertEquals(submissionMessage.getText(), "Thank you, "+name+"!");
    }

}
