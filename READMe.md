1)Functional testing: check whether link is working, able to fill and submit form, all the buttons are working as intended, navigation links are working, if user did not answer compulsory questions-> cannot proceed with submission
Performance testing:  response time of application under different conditions such as multiple user requests at the same time,large data input, speed connection and how the site behaves at peak load.
Database Testing: verify data integrity on creating, updating or deleting data in the database, obtain a correct result on executing heavy queries, retrieve data from the database and represent on the web pages correctly.
Security Testing: check if the session expires after it remains idle for a pre-defined time and testing virus attacks. It involves identifying network and system weaknesses to ensure data privacy

2) Test cases:
   1)Verify if user is able to fill his/her name in the form
   2)Verify if populate button is working
   3)Verify if user is able to select multiple checkboxes for 2nd question
   4)Verify if user is able to select only 1 option for radio buttons for 3rd question (OS qns)
   5)Verify if default value for dropdown is correct and user is able to click on it.
   6)Verify if slider and comment box is disabled when the checkbox is not selected
   7)Verify if slider and comment box is enabled when the checkbox is selected
   8)Verify if user is able to submit form after filling up the details and success message is shown

4)
SELECT e.employee_id, e.employee_name, b.employee_name AS buddy_name, s.employee_name AS supervisor_name, e.team_name
FROM employees e
JOIN employees s
ON e.supervisor_id = s.employee_id
JOIN employees b
ON e.buddy_id = b.employee_id;